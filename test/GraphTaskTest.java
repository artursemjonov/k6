import static org.junit.Assert.*;
import org.junit.Test;
import java.util.*;

/** Testklass.
 */
public class GraphTaskTest {

   @Test (timeout=20000)
   public void testEmptyGraph1() {
      GraphTask graphTask = new GraphTask();
      GraphTask.Graph graph = graphTask.new Graph("G");

      List<List<GraphTask.Vertex>> components = GraphTask.getComponentsVertices(graph);

      System.out.println("Inserted Graph: " + graph.toString());
      System.out.println("Method getDisconnectedGraphs result:");
      System.out.println(components.toString());

      assertEquals("Incorrect size of components list",0, components.size());
   }

   @Test (timeout=20000)
   public void testConnectedSimpleGraph1() {
      GraphTask graphTask = new GraphTask();
      GraphTask.Graph graph = graphTask.new Graph("G");
      GraphTask.Vertex v4 = graph.createVertex("v4");
      GraphTask.Vertex v3 = graph.createVertex("v3");
      GraphTask.Vertex v2 = graph.createVertex("v2");
      GraphTask.Vertex v1 = graph.createVertex("v1");

      createEdgeBetweenVertices(v1, v2, graph);
      createEdgeBetweenVertices(v2, v3, graph);
      createEdgeBetweenVertices(v1, v3, graph);
      createEdgeBetweenVertices(v3, v4, graph);

      List<List<GraphTask.Vertex>> components = GraphTask.getComponentsVertices(graph);

      System.out.println("Inserted Graph: " + graph.toString());
      System.out.println("Method getDisconnectedGraphs result:");
      System.out.println(components.toString());

      assertEquals("Incorrect size of components list",1, components.size());
      assertEquals("Incorrect order of vertices","[v1, v2, v3, v4]", components.get(0).toString());
   }

   @Test (timeout=20000)
   public void testDisconnectedSimpleGraph1() {
      GraphTask graphTask = new GraphTask();
      GraphTask.Graph graph = graphTask.new Graph("G");
      GraphTask.Vertex v4 = graph.createVertex("v4");
      GraphTask.Vertex v3 = graph.createVertex("v3");
      GraphTask.Vertex v2 = graph.createVertex("v2");
      GraphTask.Vertex v1 = graph.createVertex("v1");

      createEdgeBetweenVertices(v1, v2, graph);
      createEdgeBetweenVertices(v3, v4, graph);

      List<List<GraphTask.Vertex>> components = GraphTask.getComponentsVertices(graph);

      System.out.println("Inserted Graph: " + graph.toString());
      System.out.println("Method getDisconnectedGraphs result:");
      System.out.println(components.toString());

      assertEquals("Incorrect size of components list",2, components.size());
      assertEquals("Incorrect order of vertices","[v1, v2]", components.get(0).toString());
      assertEquals("Incorrect order of vertices","[v3, v4]", components.get(1).toString());
   }

   @Test (timeout=20000)
   public void testConnectedSimpleGraph2() {
      GraphTask graphTask = new GraphTask();
      GraphTask.Graph graph = graphTask.new Graph("G");
      GraphTask.Vertex v6 = graph.createVertex("v6");
      GraphTask.Vertex v5 = graph.createVertex("v5");
      GraphTask.Vertex v4 = graph.createVertex("v4");
      GraphTask.Vertex v3 = graph.createVertex("v3");
      GraphTask.Vertex v2 = graph.createVertex("v2");
      GraphTask.Vertex v1 = graph.createVertex("v1");

      createEdgeBetweenVertices(v1, v2, graph);
      createEdgeBetweenVertices(v2, v3, graph);
      createEdgeBetweenVertices(v3, v6, graph);
      createEdgeBetweenVertices(v6, v5, graph);
      createEdgeBetweenVertices(v3, v4, graph);

      List<List<GraphTask.Vertex>> components = GraphTask.getComponentsVertices(graph);

      System.out.println("Inserted Graph: " + graph.toString());
      System.out.println("Method getDisconnectedGraphs result:");
      System.out.println(components.toString());

      assertEquals("Incorrect size of components list",1, components.size());
      assertEquals("Incorrect order of vertices",
              "[v1, v2, v3, v6, v5, v4]", components.get(0).toString());
   }

   @Test (timeout=20000)
   public void testDisconnectedSimpleGraph2() {
      GraphTask graphTask = new GraphTask();
      GraphTask.Graph graph = graphTask.new Graph("G");
      GraphTask.Vertex v6 = graph.createVertex("v6");
      GraphTask.Vertex v5 = graph.createVertex("v5");
      GraphTask.Vertex v4 = graph.createVertex("v4");
      GraphTask.Vertex v3 = graph.createVertex("v3");
      GraphTask.Vertex v2 = graph.createVertex("v2");
      GraphTask.Vertex v1 = graph.createVertex("v1");

      createEdgeBetweenVertices(v1, v4, graph);
      createEdgeBetweenVertices(v1, v2, graph);
      createEdgeBetweenVertices(v4, v3, graph);
      createEdgeBetweenVertices(v6, v5, graph);

      List<List<GraphTask.Vertex>> components = GraphTask.getComponentsVertices(graph);

      System.out.println("Inserted Graph: " + graph.toString());
      System.out.println("Method getDisconnectedGraphs result:");
      System.out.println(components.toString());

      assertEquals("Incorrect size of components list",2, components.size());
      assertEquals("Incorrect order of vertices",
              "[v1, v4, v3, v2]", components.get(0).toString());
      assertEquals("Incorrect order of vertices",
              "[v5, v6]", components.get(1).toString());
   }

   @Test (timeout=20000)
   public void testDisconnectedSimpleGraph3() {
      GraphTask graphTask = new GraphTask();
      GraphTask.Graph graph = graphTask.new Graph("G");
      GraphTask.Vertex v6 = graph.createVertex("v6");
      GraphTask.Vertex v5 = graph.createVertex("v5");
      GraphTask.Vertex v4 = graph.createVertex("v4");
      GraphTask.Vertex v3 = graph.createVertex("v3");
      GraphTask.Vertex v2 = graph.createVertex("v2");
      GraphTask.Vertex v1 = graph.createVertex("v1");

      createEdgeBetweenVertices(v1, v6, graph);
      createEdgeBetweenVertices(v6, v5, graph);
      createEdgeBetweenVertices(v3, v4, graph);

      List<List<GraphTask.Vertex>> components = GraphTask.getComponentsVertices(graph);

      System.out.println("Inserted Graph: " + graph.toString());
      System.out.println("Method getDisconnectedGraphs result:");
      System.out.println(components.toString());

      assertEquals("Incorrect size of components list",3, components.size());
      assertEquals("Incorrect order of vertices",
              "[v1, v6, v5]", components.get(0).toString());
      assertEquals("Incorrect order of vertices",
              "[v2]", components.get(1).toString());
      assertEquals("Incorrect order of vertices",
              "[v3, v4]", components.get(2).toString());
   }

   @Test (timeout=20000)
   public void testEmptyGraph2() {
      GraphTask graphTask = new GraphTask();
      GraphTask.Graph graph = graphTask.new Graph("G");

      graph.createRandomSimpleGraph(0, 0, -1);
      List<List<GraphTask.Vertex>> components = GraphTask.getComponentsVertices(graph);

      System.out.println("Inserted Graph: " + graph.toString());
      System.out.println("Method getDisconnectedGraphs result:");
      System.out.println(components.toString());

      assertEquals("Incorrect size of components list",0, components.size());
   }

   @Test (timeout=20000)
   public void testConnectedRandomSimpleGraph1() {
      GraphTask graphTask = new GraphTask();
      GraphTask.Graph graph = graphTask.new Graph("G");

      graph.createRandomSimpleGraph(6, 9, -1);
      List<List<GraphTask.Vertex>> components = GraphTask.getComponentsVertices(graph);

      System.out.println("Inserted Graph: " + graph.toString());
      System.out.println("Method getDisconnectedGraphs result:");
      System.out.println(components.toString());

      assertEquals("Incorrect size of components list",1, components.size());
      assertEquals("Incorrect size of vertices list",6, components.get(0).size());
   }

   @Test (timeout=20000)
   public void testConnectedRandomSimpleGraph2() {
      GraphTask graphTask = new GraphTask();
      GraphTask.Graph graph = graphTask.new Graph("G");

      graph.createRandomSimpleGraph(2500, 2505, -1);

      Long start = System.currentTimeMillis();

      List<List<GraphTask.Vertex>> components = GraphTask.getComponentsVertices(graph);

      System.out.println(String.format(
              "Method getDisconnectedGraphs execution time: %s ms", (System.currentTimeMillis() - start)));
      System.out.println("Method getDisconnectedGraphs result size: " + components.size());
      System.out.println("Method getDisconnectedGraphs result vertices count: " + components.get(0).size());

      assertEquals("Incorrect size of components list",1, components.size());
      assertEquals("Incorrect size of components vertices list",2500, components.get(0).size());
   }

   @Test (timeout=20000)
   public void testDisconnectedRandomSimpleGraph1() {
      GraphTask graphTask = new GraphTask();
      GraphTask.Graph graph = graphTask.new Graph("G");

      graph.createRandomDisconnectedSimpleGraph(6, 3);
      List<List<GraphTask.Vertex>> components = GraphTask.getComponentsVertices(graph);

      System.out.println("Inserted Graph: " + graph.toString());
      System.out.println("Method getDisconnectedGraphs result:");
      System.out.println(components.toString());

      assertEquals("Incorrect size of components list",3, components.size());
   }

   @Test (timeout=20000)
   public void testDisconnectedRandomSimpleGraph2() {
      GraphTask graphTask = new GraphTask();
      GraphTask.Graph graph = graphTask.new Graph("G");

      graph.createRandomDisconnectedSimpleGraph(2500, 6);

      Long start = System.currentTimeMillis();

      List<List<GraphTask.Vertex>> components = GraphTask.getComponentsVertices(graph);

      System.out.println(String.format(
              "Method getDisconnectedGraphs execution time: %s ms", (System.currentTimeMillis() - start)));
      System.out.println("Method getDisconnectedGraphs result size: " + components.size());

      assertEquals("Incorrect size of components list",6, components.size());
   }

   @Test (expected=NullPointerException.class)
   public void testGraphNull() {
      GraphTask.Graph graph = null;
      List<List<GraphTask.Vertex>> components = GraphTask.getComponentsVertices(graph);
   }


   /**
    * Create an edge between 2 vertices in given graph.
    * @param first  first vertex
    * @param second  second vertex
    * @param graph  graph to which vertices belong
    */
   public void createEdgeBetweenVertices(GraphTask.Vertex first, GraphTask.Vertex second, GraphTask.Graph graph){
      graph.createArc("a" + first.toString() + "_" + second.toString(), first, second);
      graph.createArc("a" + second.toString() + "_" + first.toString(), second, first);
   }
}

