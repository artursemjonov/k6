import java.util.*;

/** Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

   /** Main method. */
   public static void main (String[] args) {
      GraphTask a = new GraphTask();
      a.run();
   }

   /** Actual main method to run examples and everything. */
   public void run() {
      Graph g = new Graph ("G");
      g.createRandomSimpleGraph (0, 0, -1);
      System.out.println ("Empty graph");
      System.out.println (g);
      List<List<Vertex>> components = getComponentsVertices(g);
      System.out.println(components.toString());
      System.out.println("Size of found components: " + components.size());
      System.out.println("---------------------------");

      System.out.println();
      g.createRandomSimpleGraph (6, 9, -1);
      System.out.println ("Connected graph with 6 vertices and 1 component");
      System.out.println (g);
      components = getComponentsVertices(g);
      System.out.println(components.toString());
      System.out.println("Size of found components: " + components.size());
      System.out.println("---------------------------");

      System.out.println();
      g.createRandomDisconnectedSimpleGraph (5,  1);
      System.out.println ("Connected graph with 1 component where each component has 1-5 vertices");
      System.out.println (g);
      components = getComponentsVertices(g);
      System.out.println(components.toString());
      System.out.println("Size of found components: " + components.size());
      System.out.println("---------------------------");

      System.out.println();
      g.createRandomDisconnectedSimpleGraph (8,  3);
      System.out.println ("Disconnected graph with 3 components where each component has 1-8 vertices");
      System.out.println (g);
      components = getComponentsVertices(g);
      System.out.println(components.toString());
      System.out.println("Size of found components: " + components.size());
      System.out.println("---------------------------");

      System.out.println();
      g.createRandomDisconnectedSimpleGraph (2500,  5);
      System.out.println ("Disconnected graph with 5 components where each component has 1-2500 vertices");
//      System.out.println (g);
      components = getComponentsVertices(g);
//      System.out.println(components.toString());
      System.out.println("Size of found components: " + components.size());
   }

   /**
    * Find components from given undirected graph by using depth-first search and return a list of components,
    * where each component is a list of components vertices.
    * Side effect: corrupts info fields in the graph.
    * @param graph undirected graph to be analyzed
    * @return List of components where each component is a list of components vertices.
    */
   public static List<List<Vertex>> getComponentsVertices(Graph graph) {
      if (graph == null) {
         throw new NullPointerException("Given input was null.");
      }

      List<List<Vertex>> components = new ArrayList<>();

      if (graph.first == null) {
         return components;
      }

      LinkedList<Vertex> graphVertices = new LinkedList<>();

      Vertex vertex = graph.first;

      while (vertex != null) {
         vertex.info = 0;
         graphVertices.add(vertex);
         vertex = vertex.next;
      }

      LinkedList<Vertex> verticesToProcess = new LinkedList<>();

      while (!graphVertices.isEmpty()) {
         List<Vertex> processedVertices = new LinkedList<>();

         Vertex processVertex = graphVertices.getFirst();
         processVertex.info = 1;
         verticesToProcess.add(processVertex);

         while (!verticesToProcess.isEmpty()) {
            processVertex = verticesToProcess.removeLast();

            Arc arc = processVertex.first;

            while (arc != null) {
               if (arc.target.info == 0) {
                  arc.target.info = 1;
                  verticesToProcess.add(arc.target);
               }

               arc = arc.next;
            }

            processedVertices.add(processVertex);
            graphVertices.remove(processVertex);
         }

         components.add(processedVertices);
      }

      return components;
   }

   class Vertex {

      private String id;
      private Vertex next;
      private Arc first;
      private int info = 0;

      Vertex (String s, Vertex v, Arc e) {
         id = s;
         next = v;
         first = e;
      }

      Vertex (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }
   }

   /** Arc represents one arrow in the graph. Two-directional edges are
    * represented by two Arc objects (for both directions).
    */
   class Arc {

      private String id;
      private Vertex target;
      private Arc next;
      private int info = 0;

      Arc (String s, Vertex v, Arc a) {
         id = s;
         target = v;
         next = a;
      }

      Arc (String s) {
         this (s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }
   }

   class Graph {

      private String id;
      private Vertex first;
      private int info = 0;

      Graph (String s, Vertex v) {
         id = s;
         first = v;
      }

      Graph (String s) {
         this (s, null);
      }

      @Override
      public String toString() {
         String nl = System.getProperty ("line.separator");
         StringBuffer sb = new StringBuffer (nl);
         sb.append (id);
         sb.append (nl);
         Vertex v = first;
         while (v != null) {
            sb.append (v.toString());
            sb.append (" -->");
            Arc a = v.first;
            while (a != null) {
               sb.append (" ");
               sb.append (a.toString());
               sb.append (" (");
               sb.append (v.toString());
               sb.append ("->");
               sb.append (a.target.toString());
               sb.append (")");
               a = a.next;
            }
            sb.append (nl);
            v = v.next;
         }
         return sb.toString();
      }

      public Vertex createVertex (String vid) {
         Vertex res = new Vertex (vid);
         res.next = first;
         first = res;
         return res;
      }

      public Arc createArc (String aid, Vertex from, Vertex to) {
         Arc res = new Arc (aid);

         res.next = from.first;
         from.first = res;
         res.target = to;
         return res;
      }

      /**
       * Create a connected undirected random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       * @param n number of vertices added to this graph
       * @param componentNoStr string representation of a component number, if null then set to empty string
       */
      public void createRandomTree (int n, String componentNoStr) {
         if (n <= 0)
            return;

         if (componentNoStr == null) {
            componentNoStr = "";
         }

         Vertex[] varray = new Vertex [n];

         for (int i = 0; i < n; i++) {
            varray [i] = createVertex ("v" + String.valueOf(n-i) + componentNoStr);

            if (i > 0) {
               int vnr = (int)(Math.random()*i);

               createArc (
                       "a" + varray [vnr].toString() + "_"+ varray [i].toString(),
                       varray [vnr],
                       varray [i]);

               createArc (
                       "a" + varray [i].toString() + "_" + varray [vnr].toString(),
                       varray [i],
                       varray [vnr]);

            } else {}
         }
      }

      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph
       * @return adjacency matrix
       */
      public int[][] createAdjMatrix() {
         info = 0;
         Vertex v = first;

         while (v != null) {
            v.info = info++;
            v = v.next;
         }

         int[][] res = new int [info][info];

         v = first;

         while (v != null) {
            int i = v.info;
            Arc a = v.first;

            while (a != null) {
               int j = a.target.info;
               res [i][j]++;
               a = a.next;
            }

            v = v.next;
         }

         return res;
      }

      /**
       * Create a connected simple (undirected, no loops, no multiple
       * arcs) random graph with n vertices and m edges.
       * @param n number of vertices
       * @param m number of edges
       * @param componentNo component number, if number is less than 0 no number will be used.
       */
      public void createRandomSimpleGraph (int n, int m, int componentNo) {
         if (n <= 0)
            return;
         if (n > 2500)
            throw new IllegalArgumentException ("Too many vertices: " + n);
         if (m < n-1 || m > n*(n-1)/2)
            throw new IllegalArgumentException 
               ("Vertices: " + n + ". Impossible number of edges: " + m);

         first = null;
         String componentNoStr = "";

         if (componentNo > -1) {
            componentNoStr = String.valueOf(componentNo);
         }

         createRandomTree (n, componentNoStr);       // n-1 edges created here

         Vertex[] vert = new Vertex [n];
         Vertex v = first;

         int c = 0;

         while (v != null) {
            vert[c++] = v;
            v = v.next;
         }

         int[][] connected = createAdjMatrix();

         int edgeCount = m - n + 1;  // remaining edges

         while (edgeCount > 0) {
            int i = (int)(Math.random()*n);  // random source
            int j = (int)(Math.random()*n);  // random target

            if (i==j) 
               continue;  // no loops

            if (connected [i][j] != 0 || connected [j][i] != 0) 
               continue;  // no multiple edges

            Vertex vi = vert [i];
            Vertex vj = vert [j];
            createArc ("a" + vi.toString() + "_" + vj.toString(), vi, vj);
            connected [i][j] = 1;
            createArc ("a" + vj.toString() + "_" + vi.toString(), vj, vi);
            connected [j][i] = 1;
            edgeCount--;  // a new edge happily created
         }
      }

      /**
       * Create a disconnected simple (undirected, no loops, no multiple arcs) random graph.
       * Each components number of vertices is chosen randomly in range of given maxVertexCount.
       * Number of edges is chosen randomly from found random vertices count.
       * @param maxVertexCount max number of vertices each component contains
       * @param componentCount number of components from 0 to 9
       */
      public void createRandomDisconnectedSimpleGraph(int maxVertexCount, int componentCount) {
         if (maxVertexCount <= 0)
            return;
         if (maxVertexCount > 2500)
            throw new IllegalArgumentException ("Too many vertices: " + maxVertexCount);
         if (componentCount < 0 || componentCount > 9) {
            throw new IllegalArgumentException("Impossible number of components: " + componentCount);
         }

         int count = 0;
         Vertex firstVertex = null;
         Vertex lastVertex = null;

         while (count < componentCount) {
            int graphVertexCount = (int)(Math.random()*maxVertexCount);

            if (graphVertexCount == 0) {
               graphVertexCount = 1;
            }

            int minNoOfVertices = graphVertexCount * (graphVertexCount - 1) / 2;
            int maxNoOfVertices = graphVertexCount - 1;

            int graphEdgeCount = (int)(Math.random()*(maxNoOfVertices - minNoOfVertices) + minNoOfVertices);

            createRandomSimpleGraph(graphVertexCount, graphEdgeCount, count);

            if (firstVertex == null) {
               firstVertex = first;
            } else {
               lastVertex.next = first;
            }

            if (first != null) {
               lastVertex = first;

               while (lastVertex.next != null) {
                  lastVertex = lastVertex.next;
               }
            }

            count++;
         }

         first = firstVertex;
      }
   }
} 

